package day1;

public class FizzBuzz {
    private int value;

    public FizzBuzz(int value) {
        this.value = value;
    }

    public String checkInputValue_v1() {
        String result = "" + this.value;

        if (this.value % 3 == 0) result = "Fizz";
        if (this.value % 5 == 0) result = "Buzz";
        if (this.value % 3 == 0 && this.value % 5 == 0) result = "FizzBuzz";
        if (this.value == 0) result = "0";

        if (check_if_contains_v1("3", result)) result = "Fizz";
        if (check_if_contains_v1("5", result)) result = "Buzz";

        return result;
    }

    public String checkInputValue_v2() {

        String result;
        switch (this.value % 15) {
            case 3:
            case 6:
            case 9:
            case 12:  // divisible by 3, print Fizz
                result = "Fizz";
                break;
            case 5:
            case 10:  // divisible by 5, print Buzz
                result = "Buzz";
                break;
            case 0:  // divisible by 3 and by 5, print FizzBuzz
                result = "FizzBuzz";
                break;
            default:  // else print the number
                result = "" + this.value;
        }

        if (check_if_contains_v1("3", result)) result = "Fizz";
        if (check_if_contains_v1("5", result)) result = "Buzz";

        return result;
    }

    public static boolean check_if_contains_v1(String searchingValue, String value) {
        return value.contains(searchingValue);
    }

    @Override
    public String toString() {
        return checkInputValue_v2();
    }
}
