package day1.threads;

import java.util.concurrent.TimeUnit;

public class ThreadPlaygroundRunnable implements Runnable {

    public String name;

    public ThreadPlaygroundRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {
            System.out.println(
                    "Thread name: " + Thread.currentThread().getName()
                    + " | name: " + name
                    + " | i: " + i
            );


        }
    }
}
