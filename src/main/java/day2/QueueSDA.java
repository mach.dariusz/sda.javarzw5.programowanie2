package day2;

public class QueueSDA<T> {

    private final T[] queue;
    private final int capacity;
    private int size;
    private int head;
    private int tail;

    public QueueSDA(int capacity) {
        this.capacity = capacity;
        this.queue = (T[]) new Object[capacity];
        this.size = 0;
        this.head = 0;
        this.tail = 0;
    }

    public int getCapacity() {
        return capacity;
    }

    public void push(T s) {

        if (isFull()) {
            throw new QueueIsFullException();
        }
        this.queue[this.head] = s;
        this.head = (this.head + 1) % this.capacity;
        this.size++;
    }

    private boolean isFull() {
        return this.size == this.capacity;
    }

    /**
     * @return number of items in queue
     */
    public int getSize() {
        return size;
    }

    /**
     * @return element at tail
     */
    public T pop() {

        if (isEmpty()) {
            throw new NoItemsInQueueException();
        }

        T returnedObject = this.queue[this.tail];
        this.queue[this.tail] = null;
        this.tail = (this.tail + 1) % this.capacity;
        this.size--;
        return returnedObject;
    }

    private boolean isEmpty() {
        return size == 0;
    }


    public String print() {
        String elements = "[ ";
        for (int i = tail; i < this.size; i++) {
            elements += this.queue[i];

            if (i + 1 != this.size) {
                elements += ", ";
            }
        }
        elements += " ]";
        return String.format("size: %d, elements: %s", this.size, elements);
    }

    public T peek() {
        return this.queue[this.tail];
    }
}
