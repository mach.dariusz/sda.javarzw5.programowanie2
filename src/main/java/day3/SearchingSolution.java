package day3;

import java.util.*;

public class SearchingSolution {

    /**
     * @return list of points, where first letter of searched word occurred
     */
    public static Points[] findAllOccurrencesOfFirstLetter(String word, char[][] array) {
        Points[] possible_occurrences = new Points[array.length * array[0].length];
        int index = 0;

        for (int col = 0; col < array.length; col++) {
            for (int row = 0; row < array[col].length; row++) {
                // System.out.println("Letter: " + array[col][row]);
                if (word.charAt(0) == array[col][row]) {
                    // we have found it, so we should add it the the array
                    possible_occurrences[index] = new Points(col, row);
                    index++;
                    // System.out.println(Arrays.toString(occurrences[index]));
                }
            }
        }

        Points[] points = new Points[index];
        System.arraycopy(possible_occurrences, 0, points, 0, index);

        return points;
    }


    /**
     * @return null if searching word not found; Coordinates if found
     */
    public static Coordinates findWordInTheDirection(String word, char[][] array, Points firstLetterOccurrence, Directions direction) {

        Coordinates coords = new Coordinates();
        coords.setStartPosition(firstLetterOccurrence);

        // e.g. Points [x: 0, y: 0]
        int col = firstLetterOccurrence.getX();
        int row = firstLetterOccurrence.getY();

        for (int i = 1; i < word.length(); i++) {
            if (
                    direction.getRow(row, i) != direction.getRowCondition(array, col)
                            && direction.getCol(col, i) != direction.getColCondition(array, row)
                            && isCharacterAtPosition(word.charAt(i), array, direction.getCol(col, i), direction.getRow(row, i))
            ) {
                // System.out.println("Letter found: " + array[col][row + i]);
                if (i + 1 == word.length()) {
                    // Last letter of searched word was found
                    coords.setEndPosition(new Points(direction.getCol(col, i), direction.getRow(row, i)));
                    // System.out.println("array: " + Arrays.toString(coords[1]));
                    return coords;
                }
            } else {
                return null;
            }
        }

        return null;
    }

    public static Map<Directions, List<Coordinates>> findAll(String word, char[][] array) {
        Map<Directions, List<Coordinates>> map = new HashMap<>();

        Points[] occurrences = SearchingSolution.findAllOccurrencesOfFirstLetter(word, array);

        for (int i = 0; i < occurrences.length; i++) { // iteration over occurrences

            for (Directions direction : Directions.values()) { // iteration over Directions
                Coordinates coordsInDirection = findWordInTheDirection(word, array, occurrences[i], direction);
                if (coordsInDirection != null) {
                    if (map.containsKey(direction)) {
                        // list already exists, just add to it and return
                        map.get(direction).add(coordsInDirection);
                    } else {
                        // create list, add to it and return
                        List<Coordinates> listOfCoordinatesInThisDirection = new ArrayList<>();
                        listOfCoordinatesInThisDirection.add(coordsInDirection);
                        map.put(direction, listOfCoordinatesInThisDirection);
                    }
                }
            }


        }

        return map;
    }


    private static boolean isCharacterAtPosition(char searchingCharacter, char[][] array, int col, int row) {
        return array[col][row] == searchingCharacter;
    }
}

class Points {
    private int x;
    private int y;

    public Points(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return String.format("[%d,%d]", getX(), getY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Points points = (Points) o;
        return x == points.x &&
                y == points.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}

class Coordinates {

    private Points startPosition;
    private Points endPosition;

    public Coordinates() {
    }

    public Coordinates(Points startPosition) {
        this.startPosition = startPosition;
    }

    public Coordinates(Points startPosition, Points endPosition) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    public void setStartPosition(Points startPosition) {
        this.startPosition = startPosition;
    }

    public void setEndPosition(Points endPosition) {
        this.endPosition = endPosition;
    }

    public Points getStartPosition() {
        return startPosition;
    }

    public Points getEndPosition() {
        return endPosition;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "startPosition=" + getStartPosition() +
                ", endPosition=" + getEndPosition() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return Objects.equals(startPosition, that.startPosition) &&
                Objects.equals(endPosition, that.endPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startPosition, endPosition);
    }
}

enum Directions {
    RIGHT { // row + 1

        public int getCol(int col, int i) {
            return col;
        }

        public int getRow(int row, int i) {
            return row + i;
        }

        public int getRowCondition(char[][] array, int col) {
            return array[col].length;
        }

        public int getColCondition(char[][] array, int row) {
            return array[row].length;
        }
    },
    LEFT { // row - 1

        public int getCol(int col, int i) {
            return col;
        }

        public int getRow(int row, int i) {
            return row - i;
        }

        public int getRowCondition(char[][] array, int col) {
            return -1;
        }

        public int getColCondition(char[][] array, int row) {
            return -1;
        }
    },
    TOP { // col - 1

        public int getCol(int col, int i) {
            return col - i;
        }

        public int getRow(int row, int i) {
            return row;
        }

        public int getRowCondition(char[][] array, int col) {
            return -1;
        }

        public int getColCondition(char[][] array, int row) {
            return -1;
        }
    },
    BOTTOM { // col + 1

        public int getCol(int col, int i) {
            return col + i;
        }

        public int getRow(int row, int i) {
            return row;
        }

        public int getRowCondition(char[][] array, int col) {
            return array[col].length;
        }

        public int getColCondition(char[][] array, int row) {
            return array[row].length;
        }
    },
    RIGHT_TOP { // row + 1, col - 1

        public int getCol(int col, int i) {
            return TOP.getCol(col, i);
        }

        public int getRow(int row, int i) {
            return RIGHT.getRow(row, i);
        }

        public int getRowCondition(char[][] array, int col) {
            return RIGHT.getRowCondition(array, col);
        }

        public int getColCondition(char[][] array, int row) {
            return TOP.getColCondition(array, row);
        }
    },
    RIGHT_BOTTOM { // row + 1, col + 1

        public int getCol(int col, int i) {
            return BOTTOM.getCol(col, i);
        }

        public int getRow(int row, int i) {
            return RIGHT.getRow(row, i);
        }

        public int getRowCondition(char[][] array, int col) {
            return RIGHT.getRowCondition(array, col);
        }

        public int getColCondition(char[][] array, int row) {
            return BOTTOM.getColCondition(array, row);
        }
    },
    LEFT_TOP { // row - 1, col - 1

        public int getCol(int col, int i) {
            return TOP.getCol(col, i);
        }

        public int getRow(int row, int i) {
            return LEFT.getRow(row, i);
        }

        public int getRowCondition(char[][] array, int col) {
            return LEFT.getRowCondition(array, col);
        }

        public int getColCondition(char[][] array, int row) {
            return TOP.getColCondition(array, row);
        }
    },
    LEFT_BOTTOM { // row - 1, col + 1

        public int getCol(int col, int i) {
            return BOTTOM.getCol(col, i);
        }

        public int getRow(int row, int i) {
            return LEFT.getRow(row, i);
        }

        public int getRowCondition(char[][] array, int col) {
            return LEFT.getRowCondition(array, col);
        }

        public int getColCondition(char[][] array, int row) {
            return BOTTOM.getColCondition(array, row);
        }
    };

    public int getCol(int col, int i) {
        throw new AbstractMethodError();
    }

    public int getRow(int row, int i) {
        throw new AbstractMethodError();
    }

    public int getRowCondition(char[][] array, int col) {
        throw new AbstractMethodError();
    }

    public int getColCondition(char[][] array, int row) {
        throw new AbstractMethodError();
    }
}