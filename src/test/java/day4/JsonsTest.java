package day4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class JsonsTest {

    private Car car;
    private Car[] array;

    private ObjectMapper objectMapper;
    private Gson gson;

    @BeforeEach
    void setUp() {
        car = new Car("VW", "Passat", "1.9 TDI", "diesel", 30_000.00);
        array = new Car[]{car, car, car, car};

        objectMapper = new ObjectMapper();
        gson = new Gson();
    }

    @Test
    void testJSONwithJackson() {
        String json = null;
        try {
            json = Jsons.toJSONwithJackson(car);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println(json);

        Car car2 = null;
        try {
            car2 = objectMapper.readValue(json, Car.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(car, car2);

        try {
            System.out.println(Jsons.toJSONwithJackson(array));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(Jsons.toJSONwithJacksonPretty(array));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testJSONwithGSON() {
        String json = Jsons.toJSONwithGSON(car);
        System.out.println(json);

        Car car2 = gson.fromJson(json, Car.class);

        Assertions.assertEquals(car, car2);
    }

    @Test
    void testJSONwithGSONPretty() {
        String json = Jsons.toJSONwithGSONPretty(array);
        System.out.println(json);

        Car[] cars = gson.fromJson(json, Car[].class);

        Assertions.assertArrayEquals(array, cars);
    }

}