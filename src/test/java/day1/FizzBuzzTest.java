package day1;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

public class FizzBuzzTest {

    @Test
    public void if_shouldBeNumber() {
        Assert.assertEquals(
                new FizzBuzz(1).toString(),
                "1"
        );
        Assert.assertEquals(
                new FizzBuzz(4).toString(),
                "4"
        );
    }

    @Test
    public void if_dividedBy3_Fizz() {
        Assert.assertEquals(
                new FizzBuzz(3).toString(),
                "Fizz"
        );
    }

    @Test
    public void if_dividedBy5_Buzz() {
        Assert.assertEquals(
                new FizzBuzz(5).toString(),
                "Buzz"
        );
    }

    @Test
    public void if_dividedBy3and5_FizzBuzz() {
        Assert.assertEquals(
                new FizzBuzz(15).toString(),
                "FizzBuzz"
        );
    }

    @Test
    public void if_ifContains3_Fizz() {
        Assert.assertEquals(
                new FizzBuzz(12345).toString(),
                "Fizz"
        );
    }

    @Test
    public void if_ifContains5_Buzz() {
        Assert.assertEquals(
                new FizzBuzz(12345).toString(),
                "Buzz"
        );
    }

    @Test
    public void test_checkInputValue_v1() {
        Instant start = Instant.now();

        for (int i = 0; i < 1_000_000; i++) {
            // System.out.println(new FizzBuzz(i));
            new FizzBuzz(i).checkInputValue_v1();
        }

        Instant end = Instant.now();

        long duration = Duration.between(start, end).toMillis();
        System.out.println("test_checkInputValue_v1: Duration: " + duration + " milliseconds");
        //
        // *******************************************************************************************
        //
        start = Instant.now();

        for (int i = 0; i < 1_000_000; i++) {
            new FizzBuzz(i).checkInputValue_v2();
        }

        end = Instant.now();

        duration = Duration.between(start, end).toMillis();
        System.out.println("test_checkInputValue_v2: Duration: " + duration + " milliseconds");

    }


    @Test
    public void check_if_contains_v1() {
        Assert.assertTrue(FizzBuzz.check_if_contains_v1("3", "1245678390"));
    }

}
