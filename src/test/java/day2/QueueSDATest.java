package day2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class QueueSDATest {

    @Test
    public void test1_createFifo() {
        QueueSDA queue = new QueueSDA(10);
        Assert.assertEquals(queue.getCapacity(), 10);
    }

    @Test()
    public void test2_addItemToFifo() {
        QueueSDA queue = new QueueSDA(10);
        queue.push("Kurs - Programowanie 2");
        queue.push("Kurs - Programowanie 2 - day 2");

        Assert.assertEquals(queue.getSize(), 2);

        Assert.assertEquals(queue.pop(), "Kurs - Programowanie 2");
        Assert.assertEquals(queue.pop(), "Kurs - Programowanie 2 - day 2");
    }

    @Test(expected = NoItemsInQueueException.class)
    public void test3_pop_throwException_ifFifoEmpty() {
        QueueSDA queue = new QueueSDA(10);
        queue.pop(); // throw exception
    }

    @Test(expected = QueueIsFullException.class)
    public void test4_push_throwException_ifFifoFull() {
        QueueSDA queue = new QueueSDA(5);
        queue.push("A");
        queue.push("B");
        queue.push("C");
        queue.push("D");
        queue.push("E");
        queue.push("X"); // throw exception here
    }

    @Test
    public void test5_peek() {
        QueueSDA queue = new QueueSDA(2);
        queue.push("A");

        Assert.assertEquals(queue.peek(), "A");
        Assert.assertEquals(queue.getSize(), 1);
    }

    @Test
    public void test6_print() {
        QueueSDA queue = new QueueSDA(2);
        queue.push("A");
        queue.push("B");

        Assert.assertEquals(
                queue.print(),
                "size: 2, elements: [ A, B ]"
        );

    }

    @Test
    public void test7_addMultipleTypesToQueue() {
        QueueSDA queue = new QueueSDA(5);
        queue.push("A");
        queue.push(1);
        queue.push(new Date());

        Assert.assertEquals(queue.getSize(), 3);

        QueueSDA<Integer> queueIntegers = new QueueSDA<>(4);
        QueueSDA<Double> queueDoubles = new QueueSDA<>(6);
        QueueSDA<String> queueStrings = new QueueSDA<>(7);
        QueueSDA<Date> queueDates = new QueueSDA<>(8);

        Assert.assertEquals(queueIntegers.getCapacity(), 4);
        Assert.assertEquals(queueDoubles.getCapacity(), 6);
        Assert.assertEquals(queueStrings.getCapacity(), 7);
        Assert.assertEquals(queueDates.getCapacity(), 8);
    }


    @Test
    public void test8_addingAndRemovingItems_moreAddsThanCapacity() {
        QueueSDA<Integer> queue = new QueueSDA<>(5);
        queue.push(1); // head 1
        queue.push(2); // head 2
        queue.push(3); // head 3
        queue.push(4); // head 4
        queue.push(5); // head 0
        queue.pop(); // tail 1
        queue.push(6);  // head 1
        queue.pop(); // tail 2
        queue.push(7);  // head 2
        queue.pop(); // tail 3
        queue.pop(); // tail 4
        queue.pop(); // tail 0
        queue.pop(); // tail 1
        queue.pop(); // tail 2
    }


}
